const express = require('express')
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const User = require('../models/user')
const router = express.Router()

router.post('/signup', (req, res) => {
  bcrypt
    .hash(req.body.password, 10)
    .then(hash => {
      const user = User.create({
        email: req.body.email,
        password: hash
      })

      user
        .then(result => {
          console.log(result)
          res.status(200).json({ success: 'new user has been created' })
        })
        .catch(err => {
          res.status(500).json({ error: err })
        })
    })
    .catch(err => {
      res.status(500).json({ error: 'asem' })
    })
})

router.post('/login', (req, res, next) => {
  const user = User.findOne({ email: req.body.email }).exec()
  user
    .then(result => {
      bcrypt
        .compare(req.body.password, result.password)
        .then(result => {
          const jwtToken = jwt.sign(
            {
              email: result.email,
              _id: result.id
            },
            'somesecrethere',
            {
              expiresIn: '2h'
            }
          )
          res.status(200).json({ success: 'Login Success' })
        })
        .catch(err => {
          res.status(401).json({ failed: 'Unauthorized access' })
        })
    })
    .catch(err => {
      res.status(500).json({ error: err })
    })
})

module.exports = router
